package com.cotefacil.userservice;

import com.cotefacil.userservice.domain.Role;
import com.cotefacil.userservice.domain.User;
import com.cotefacil.userservice.service.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;

@SpringBootApplication
public class UserserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserserviceApplication.class, args);
	}

	@Bean
	PasswordEncoder passwordEncoder(){
		return new BCryptPasswordEncoder();
	}


	@Bean
	CommandLineRunner run(UserService userService){
		return args -> {
			userService.saveRole(new Role(null, "ROLE_USER"));
			userService.saveRole(new Role(null, "ROLE_MANAGER"));
			userService.saveRole(new Role(null, "ROLE_ADMIN"));
			userService.saveRole(new Role(null, "ROLE_SUPER_ADMIN"));

			userService.saveUser(new User(null, "Bob Marley", "bob", "1234", new ArrayList<>()));
			userService.saveUser(new User(null, "Cris Barnes", "cris", "1234", new ArrayList<>()));
			userService.saveUser(new User(null, "Tico Teco", "tico", "1234", new ArrayList<>()));
			userService.saveUser(new User(null, "Maria Betania", "maria", "1234", new ArrayList<>()));

			userService.addRoleToUser("bob", "ROLE_USER");
			userService.addRoleToUser("bob", "ROLE_MANAGER");
			userService.addRoleToUser("cris", "ROLE_MANAGER");
			userService.addRoleToUser("tico", "ROLE_ADMIN");
			userService.addRoleToUser("maria", "ROLE_SUPER_ADMIN");
			userService.addRoleToUser("maria", "ROLE_ADMIN");
			userService.addRoleToUser("maria", "ROLE_USER");
		};
	}

}
