package com.cotefacil.userservice.service;

import com.cotefacil.userservice.domain.Role;
import com.cotefacil.userservice.domain.User;

import java.util.List;

public interface UserService {

    User saveUser(User user);
    Role saveRole(Role role);

    void addRoleToUser(String userName, String roleName);
    User getUser(String userName);
    List<User> getUsers();

}
