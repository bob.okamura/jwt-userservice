package com.cotefacil.userservice.repository;

import com.cotefacil.userservice.domain.Role;
import com.cotefacil.userservice.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {

    Role findByName(String name);
}
